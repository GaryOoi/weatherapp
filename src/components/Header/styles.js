import styled from "styled-components";

export const NavHeader = styled.h1`
  color: #fff;
  line-height: 1.2;
  font-size: 32px;
  font-weight: 800;
`;
