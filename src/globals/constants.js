export const API_KEY = '7a4dc385469efcf6fdfffae42a3a7b7b';

export const DEFAULT_WEATHER_WATCHLIST = [
  { city: 'singapore', refreshTime: '60000' },
];

export const REFRESH_TIME_OPTIONS = [
  { label: '5 seconds', value: 5000 },
  { label: '10 seconds', value: 10000 },
  { label: '30 seconds', value: 30000 },
  { label: '1 minute', value: 60000 },
];

export const WEATHER_COLOR = {
  'Clear sky': '#FF5722',
  'Few clouds': '#FF5722',
  'Scattered clouds': '#607D8B',
  'Broken clouds': '#607D8B',
  'Shower rain': '#6200ee',
  Rain: '#6200ee',
  Thunderstorm: '#6200ee',
  Snow: '#2979FF',
  Mist: '#2979FF',
};
