import React, { useState, useEffect } from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';

import { fetchCurrentData, fetchForecastData } from 'utils/request';
import { WEATHER_COLOR } from 'globals/constants';
import {
  Overlay,
  ButtonWrapper,
  ColoredCard,
  PlaceholderCard,
  CardTitle,
  CardSubtitle,
  CardImage,
  CardContent,
  CardFooterText,
} from './styles';

function WeatherCard({ city: { city, refreshTime }, handleDeleteCityWeather }) {
  const [currentData, setCurrentData] = useState({});
  const [forecastData, setForecastData] = useState([]);
  const [refresh, setRefresh] = useState(0);

  useEffect(() => {
    const fetchAPI = async () => {
      const data = await fetchCurrentData(city);
      if (data.error) {
        clearInterval(interval);
      }
      setCurrentData(await fetchCurrentData(city));
    };

    const fetchForecasrAPI = async () => {
      setForecastData(await fetchForecastData(city));
    };

    fetchForecasrAPI();
    fetchAPI();

    let interval = setInterval(() => fetchAPI(), refreshTime);
    return () => {
      clearInterval(interval);
    };
  }, [refresh, city, refreshTime]);

  const handleRefresh = () => {
    setRefresh(refresh + 1);
  };

  return currentData &&
    forecastData &&
    !currentData.error &&
    !forecastData.error ? (
    <ColoredCard color={WEATHER_COLOR[currentData.description]}>
      <Card.Body>
        <CardTitle className="text-center">{currentData.description}</CardTitle>
        <CardSubtitle className="text-center">
          {currentData.city}, {currentData.country}
        </CardSubtitle>
        <div className="d-flex align-items-center justify-content-center">
          <CardImage
            className="align-item-center"
            src={`http://openweathermap.org/img/w/${currentData.iconCode}.png`}
          />
        </div>

        <CardContent className="text-center mb-5">
          {currentData.temperature}&#176;
        </CardContent>
        <Row>
          {forecastData.map((forecast) => {
            return (
              <Col
                key={forecast.day}
                className="d-flex flex-column align-items-center justify-content-center"
              >
                <CardFooterText>{forecast.day}</CardFooterText>
                <img
                  className="align-item-center"
                  src={`http://openweathermap.org/img/w/${forecast.iconCode}.png`}
                  alt="Weather"
                />
                <CardFooterText>{forecast.temperature}&#176;</CardFooterText>
              </Col>
            );
          })}
        </Row>
      </Card.Body>
      <Overlay />
      <ButtonWrapper>
        <Button
          variant="outline-light"
          onClick={() => handleDeleteCityWeather(city)}
        >
          Remove
        </Button>
      </ButtonWrapper>
    </ColoredCard>
  ) : (
    <PlaceholderCard>
      <Card.Body className="d-flex flex-column align-items-center justify-content-center">
        <h1>Oops!</h1>
        <h2>404 Not Found</h2>
        <p>Sorry, an error has occured. Requested city not found!</p>
      </Card.Body>
      <Overlay />
      <ButtonWrapper>
        <Button
          className="mr-2"
          variant="outline-light"
          onClick={() => handleRefresh()}
        >
          Refresh Again
        </Button>
        <Button
          variant="outline-light"
          onClick={() => handleDeleteCityWeather(city)}
        >
          Remove
        </Button>
      </ButtonWrapper>
    </PlaceholderCard>
  );
}

WeatherCard.propTypes = {
  city: PropTypes.object.isRequired,
  handleDeleteCityWeather: PropTypes.func.isRequired,
};

export default WeatherCard;
