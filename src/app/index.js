import React from 'react';
import { Helmet } from 'react-helmet';
import 'bootstrap/dist/css/bootstrap.min.css';

import Home from 'pages/Home';
import Header from 'components/Header';
import Footer from 'components/Footer';
import { FlexGrowDiv } from './styles';

function App() {
  return (
    <>
      <Helmet titleTemplate="%s - Weather App" defaultTitle="">
        <meta
          name="description"
          content="View and forecast your favourite city weather"
        />
      </Helmet>
      <Header />
      <FlexGrowDiv>
        <Home />
      </FlexGrowDiv>
      <Footer />
    </>
  );
}

App.propTypes = {};

export default App;
