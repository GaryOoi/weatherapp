import React, { useState } from 'react';
import { Formik } from 'formik';
import * as yup from 'yup';
import {
  Container,
  Row,
  Col,
  Alert,
  Form,
  Button,
  Modal,
} from 'react-bootstrap';

import WeatherCard from 'components/WeatherCard';
import {
  DEFAULT_WEATHER_WATCHLIST,
  REFRESH_TIME_OPTIONS,
} from 'globals/constants';
import { Section, SectionHeader, FormContentWrapper } from './styles';

const initialState = {
  city: '',
  refreshTime: '',
};

const schema = yup.object({
  city: yup.string().required(),
  refreshTime: yup.number().required(),
});

function Home() {
  const [weatherWatchlist, setWeatherWatchlist] = useState(
    DEFAULT_WEATHER_WATCHLIST,
  );
  const [showModal, setShowModal] = useState(false);
  const [modalContent, setModalContent] = useState('');

  const handleSubmit = (values, { resetForm }) => {
    // return true if found duplicate
    const isDuplicate = weatherWatchlist.some(
      (cityWeather) => cityWeather.city === values.city,
    );

    if (weatherWatchlist.length < 3 && !isDuplicate) {
      setWeatherWatchlist([...weatherWatchlist, values]);
    } else if (isDuplicate) {
      setShowModal(true);
      setModalContent('Duplicate city!');
    } else {
      setShowModal(true);
      setModalContent('Sorry, cannot add more than 3 cities!');
    }

    resetForm({});
  };

  const handlePressModal = () => {
    setShowModal(!showModal);
  };

  const handleDeleteCityWeather = (city) => {
    setWeatherWatchlist(
      weatherWatchlist.filter((cityWeatherObj) => {
        return cityWeatherObj.city !== city;
      }),
    );
  };

  return (
    <>
      <Section>
        <Container>
          <SectionHeader className="mb-4">Weather Watchlist</SectionHeader>
          <Row className="mb-5">
            {weatherWatchlist.length !== 0 ? (
              weatherWatchlist.map((cityWeather) => {
                return (
                  <Col key={cityWeather.city}>
                    <WeatherCard
                      key={cityWeather.city}
                      city={cityWeather}
                      handleDeleteCityWeather={handleDeleteCityWeather}
                    />
                  </Col>
                );
              })
            ) : (
              <Alert className="w-100" variant="warning">
                <Alert.Heading>Oh snap! No data to display!</Alert.Heading>
                <p>
                  Add your city through the form below to view the latest
                  weather data.
                </p>
              </Alert>
            )}
          </Row>
          <SectionHeader className="mb-4 text-center">
            Add Weather Watchlist
          </SectionHeader>
          <Formik
            validationSchema={schema}
            onSubmit={handleSubmit}
            initialValues={initialState}
          >
            {/* eslint-disable-next-line no-shadow */}
            {({ handleSubmit, handleChange, values, errors }) => (
              <Form noValidate onSubmit={handleSubmit}>
                <FormContentWrapper>
                  <Form.Group controlId="formBasicEmail">
                    <Form.Label>City</Form.Label>
                    <Form.Control
                      placeholder="Enter City (kuala lumpur, london, etc)"
                      name="city"
                      value={values.city}
                      onChange={handleChange}
                      isInvalid={!!errors.city}
                    />
                    <Form.Control.Feedback type="invalid">
                      {errors.city}
                    </Form.Control.Feedback>
                  </Form.Group>

                  <Form.Group controlId="formGridState">
                    <Form.Label>Refresh Interval</Form.Label>
                    <Form.Control
                      as="select"
                      name="refreshTime"
                      value={values.refreshTime || -1}
                      onChange={handleChange}
                      isInvalid={!!errors.refreshTime}
                    >
                      <option value="">Select...</option>
                      {REFRESH_TIME_OPTIONS.map((option) => {
                        return (
                          <option key={option.label} value={option.value}>
                            {option.label}
                          </option>
                        );
                      })}
                    </Form.Control>
                    <Form.Control.Feedback type="invalid">
                      {errors.refreshTime}
                    </Form.Control.Feedback>
                  </Form.Group>
                  <Button
                    className="mt-5"
                    variant="primary"
                    type="submit"
                    block
                  >
                    Add City
                  </Button>
                </FormContentWrapper>
              </Form>
            )}
          </Formik>
        </Container>
      </Section>
      <Modal show={showModal} onHide={() => handlePressModal()}>
        <Modal.Header closeButton>
          <Modal.Title>Alert Message</Modal.Title>
        </Modal.Header>
        <Modal.Body className="text-danger">{modalContent}</Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={() => handlePressModal()}>
            Noted
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default Home;
