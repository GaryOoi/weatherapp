import React from "react";
import { Navbar, Container } from "react-bootstrap";

import { NavHeader } from "./styles";

function Header() {
  return (
    <Navbar bg="dark" variant="dark">
      <Container>
        <NavHeader href="#home">Weather App</NavHeader>
      </Container>
    </Navbar>
  );
}

export default Header;
