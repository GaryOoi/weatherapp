import styled from "styled-components";

export const Section = styled.div`
  padding: 3em 0;
  position: relative;
  @media (max-width: 767.98px) {
    padding-top: 4em;
  }
`;

export const SectionHeader = styled.h2`
  font-size: 30px;
  font-weight: 800;
`;

export const FormContentWrapper = styled.div`
  max-width: 530px;
  margin: auto;
`;
