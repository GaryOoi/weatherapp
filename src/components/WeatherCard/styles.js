import styled from 'styled-components';
import { Card } from 'react-bootstrap';

export const Overlay = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
  content: '';
  position: absolute;
  z-index: 1;
  background: #000;
  opacity: 0;
  border-radius: 0.25rem;
  transition: all 0.3s ease;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
  content: '';
  position: absolute;
  z-index: 2;
  background: #ffffff00;
  opacity: 0;
  border-radius: 0.25rem;
`;

export const ColoredCard = styled(Card)`
  background-color: ${(props) => (props.color ? props.color : '#03da')};
  border: 0px;

  &:hover > ${Overlay} {
    opacity: 0.5;
  }
  &:hover > ${ButtonWrapper} {
    opacity: 1;
  }
`;

export const PlaceholderCard = styled(ColoredCard)`
  background-color: #cfd8dc;
  width: 100%;
  height: 100%;
`;

export const CardTitle = styled.h3`
  font-size: 24px;
  color: white;
`;

export const CardSubtitle = styled.p`
  font-size: 18px;
  color: white;
`;

export const CardImage = styled.img`
  height: 70px;
  width: 70px;
`;

export const CardContent = styled.h1`
  font-size: 56px;
  font-weight: 700;
  color: white;
`;

export const CardFooterText = styled.p`
  font-size: 16px;
  color: white;
`;
