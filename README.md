﻿\*\*Check if you have node and npm installed. Make sure you have LTS node 10 or above.

1. Run "npm install" to install all the dependencies.
2. Run "npm start" to start the weather app.
3. To add city - Fill in the form and press "Add City" Button.
4. To remove weather watchlist - Hover on the card, a remove button will appear. Press on the remove button to remove it from watchlist.
5. If error occur on the request city:
   a) Hover on the error card, "Refresh" and "Remove" button will appear.
   b) Press "Refresh" to request the weather data again.
   c) Press "Remove" to remove the city in the watchlist.
