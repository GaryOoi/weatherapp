import { API_KEY } from '../globals/constants';

const url = 'https://api.openweathermap.org/data/2.5';

/**
 * @description Fetch city current weather data from https://api.openweathermap.org.
 * @param {string} city City name
 */
export const fetchCurrentData = async (city) => {
  try {
    const response = await fetch(
      `${url}/weather?q=${city}&appid=${API_KEY}&units=metric`,
    );

    const responseJson = await response.json();

    const modifiedData = {
      temperature: responseJson.main.temp,
      city: responseJson.name,
      country: responseJson.sys.country,
      description:
        responseJson.weather[0].description.charAt(0).toUpperCase() +
        responseJson.weather[0].description.slice(1),
      iconCode: responseJson.weather[0].icon,
    };

    return modifiedData;
  } catch (error) {
    return {
      error: true,
    };
  }
};

/**
 * @description Fetch city 5 days weather forecast data from https://api.openweathermap.org. *
 * @param {string} city City name
 */
export const fetchForecastData = async (city) => {
  try {
    const response = await fetch(
      `${url}/forecast?q=${city}&appid=${API_KEY}&units=metric`,
    );

    const responseJson = await response.json();

    const modifiedForecastData = [];
    const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat'];

    for (let i = 0; i < 40; i += 8) {
      const d = new Date(responseJson.list[i].dt * 1000);
      modifiedForecastData.push({
        day: days[d.getDay()],
        temperature: responseJson.list[i].main.temp,
        iconCode: responseJson.list[i].weather[0].icon,
      });
    }

    return modifiedForecastData;
  } catch (error) {
    return {
      error: true,
    };
  }
};
